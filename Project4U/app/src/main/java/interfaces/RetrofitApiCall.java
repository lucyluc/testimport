package interfaces;

import com.google.gson.JsonObject;

import models.CheckInStatusResult;
import models.ReservableUnitResult;
import models.ReservationResult;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

public interface RetrofitApiCall {
    @GET
    Call<ReservableUnitResult> getReservableUnit(@Url String url);

    @GET
    Call<ReservationResult> getAllReservations(@Url String url);

    @PUT
    Call<CheckInStatusResult> checkMeetingStatus(@Url String url);

    @GET
    Call<JsonObject> authenticate(@Url String url);

    @GET
    Call<JsonObject> getInstanceProperty(@Url String url);

    @PATCH
    Call<JsonObject> registerPushyToken(@Url String url, @Body JsonObject body);

    @POST
    Call<ResponseBody> postVoiceTranslation(@Url String url, @Body RequestBody params);

}
