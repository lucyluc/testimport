package interfaces;

import java.util.ArrayList;

public interface ResponseCallBack {
    int RESPONSE_TYPE_RESERVATIONS = 0;
    int RESPONSE_TYPE_RESERVABLE_UNIT = 1;
    int RESPONSE_TYPE_CHECKIN_STATUS = 2;
    int RESPONSE_TYPE_INSTANCE_PROPERTY_SYNC_ON_UPDATE = 3;
    int RESPONSE_TYPE_INSTANCE_PROPERTY_DARK_THEME = 4;
    int RESPONSE_TYPE_INSTANCE_PROPERTY_SPEECH_TO_TEXT = 5;
    int RESPONSE_TYPE_INSTANCE_PROPERTY_LOGIN_CODE = 6;
    int RESPONSE_TYPE_AUTHENTICATE = 7;

    void onCallSuccessListResult(ArrayList result, int responseType);
    void onCallSuccessObjectResult(Object object, int responseType);
    void OnCallFailed(int responseType);
}
