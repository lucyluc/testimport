package activities;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.bing.speech.SpeechClientStatus;
import com.microsoft.cognitiveservices.speechrecognition.DataRecognitionClient;
import com.microsoft.cognitiveservices.speechrecognition.ISpeechRecognitionServerEvents;
import com.microsoft.cognitiveservices.speechrecognition.MicrophoneRecognitionClient;
import com.microsoft.cognitiveservices.speechrecognition.RecognitionResult;
import com.microsoft.cognitiveservices.speechrecognition.RecognitionStatus;
import com.microsoft.cognitiveservices.speechrecognition.SpeechRecognitionMode;
import com.microsoft.cognitiveservices.speechrecognition.SpeechRecognitionServiceFactory;
import com.plat4mation.project4u.*;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONException;

import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;

import adapters.ReservationAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import dialogs.ViewListeningDialog;
import fragments.ConfigurationDialogFragment;
import handlers.ApiCallHandler;
import handlers.DeviceLedHandler;
import handlers.NfcHandler;
import interfaces.ResponseCallBack;
import me.pushy.sdk.Pushy;
import models.CheckInStatus;
import models.ReservableUnit;
import models.Reservation;
import receivers.PushReceiver;
import util.UnitManager;
import util.Utils;

public class MainActivity extends AppCompatActivity implements ResponseCallBack, ISpeechRecognitionServerEvents {

    private static final String TAG = "MainActivity";
    public static final String NAME_SHARED_PREFERENCES = "sharedPrefs";
    private static final int DELAY_SHOW_DATA = 10000;
    private static final int DELAY_MINUTE = 60000;
    private static final int STATUS_VACANT = 0;
    private static final int STATUS_OCCUPIED = 1;

    @BindView(R.id.main_recycler_view_agenda)
    RecyclerView mRecyclerView;
    @BindView(R.id.main_text_clock_time)
    TextClock mTextClockTime;
    @BindView(R.id.main_text_clock_date)
    TextClock mTextClockDate;
    @BindView(R.id.main_text_view_availability)
    TextView mTextViewAvailability;
    @BindView(R.id.main_text_view_item_id)
    TextView mTextViewReservableUnitName;
    @BindView(R.id.main_text_view_notification)
    TextView mTextViewNotification;
    @BindView(R.id.main_text_view_check_in_status)
    TextView mTextViewCheckInStatus;
    @BindView(R.id.main_text_view_error_text)
    TextView mTextViewErrorText;
    @BindView(R.id.main_image_button_speech)
    ImageButton mImageButtonSpeech;
    @BindView(R.id.main_constraint_layout_data)
    ConstraintLayout mConstraintLayoutData;
    @BindView(R.id.main_constraint_layout_notifications)
    ConstraintLayout mConstraintLayoutNotifications;
    @BindView(R.id.main_constraint_layout_reservations)
    ConstraintLayout mConstraintLayoutReservations;
    @BindView(R.id.main_constraint_layout_error_message)
    ConstraintLayout mConstraintLayoutErrorMessage;

    private ArrayList<Reservation> mReservationList;
    private NfcHandler mNfcHandler;
    private ReservationAdapter mReservationAdapter;
    private NfcAdapter mNfcAdapter;
    private Resources mResources;
    private ConfigurationDialogFragment mDialogFragment;
    private String mSystemId;
    private String mSystemName;
    private String mCurrentReservation;
    private PendingIntent mPendingIntent;
    private PushReceiver mBroadcastReceiver;
    private IntentFilter mFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    private Handler mCheckinStatusHandler;
    private boolean mThemeIsDark;
    private boolean mSpeechToTextIsEnable;
    private boolean mSyncOnUpdate;
    private boolean mCheckedInStatus;
    private boolean mReceiverRegistered;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private int comparedTimeDifferent;

    //SpeechToText
    private int m_waitSeconds = 0;
    private DataRecognitionClient dataClient = null;
    private MicrophoneRecognitionClient micClient = null;
    private FinalResponseStatus isReceivedResponse = FinalResponseStatus.NotReceived;
    private ViewListeningDialog listeningDialog = new ViewListeningDialog();
    private RippleBackground rippleBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences = getSharedPreferences(NAME_SHARED_PREFERENCES, MODE_PRIVATE);
        Utils.setThemePreferences(this, mSharedPreferences.getBoolean("enable_dark_theme", true));

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this); //Bind view

        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);

        if (mSharedPreferences.getBoolean("enable_sync_on_update", false)) {
            this.registerReceiver(mBroadcastReceiver, mFilter);
            mReceiverRegistered = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
        }

        if (mReceiverRegistered) {
            mReceiverRegistered = false;
            this.unregisterReceiver(mBroadcastReceiver);
        }
    }

    /**
     * This method handleSyncOnUpdate everything to be done when the application started.
     */
    public void init() {
        rippleBackground = findViewById(R.id.content);

        if (mSharedPreferences.getBoolean("enable_sync_on_update", false)) {
            Pushy.listen(this);

            mBroadcastReceiver = new PushReceiver();
            mBroadcastReceiver.setMainActivity(this);
        }

        mCheckinStatusHandler = new Handler();
        mEditor = getSharedPreferences(NAME_SHARED_PREFERENCES, MODE_PRIVATE).edit();

        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING), 0);

        mResources = getResources();

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mNfcHandler = NfcHandler.getInstance();

        Utils.hideSystemUI(this); //hiding system bar
        Utils.dateFormatConfiguration(this, mTextClockTime, mTextClockDate); //Configure Date Format

        mSystemId = mSharedPreferences.getString("systemId", null);
        mSystemName = mSharedPreferences.getString("systemName", null);
        mCheckedInStatus = mSharedPreferences.getBoolean("systemStatus", false);
        mCurrentReservation = mSharedPreferences.getString("reservationEndTime", null);

        if (mSystemId == null) {
            if (Utils.hasInternetConnection(this)) {
                ApiCallHandler.getInstance().getReservableUnit(this, this);
            } else {
                showRelativeErrorMessage(getString(R.string.no_internet_connection), false);
            }
        } else {
            getReservations();
            new RegisterForPushNotificationsAsync().execute();
        }


        mThemeIsDark = mSharedPreferences.getBoolean("enable_dark_theme", true);
        mSyncOnUpdate = mSharedPreferences.getBoolean("enable_sync_on_update", false);
        mSpeechToTextIsEnable = mSharedPreferences.getBoolean("enable_speech_to_text", false);

        updateCurrentState();
        handleSyncOnUpdate();

        setSpeechPreferences(mSpeechToTextIsEnable);
        mTextViewReservableUnitName.setText(mSystemName);
    }

    /**
     * Redirect after button long click to another activity.
     */
    @OnLongClick(R.id.main_image_button_configuration)
    public boolean OpenConfigurationDialog() {
        mDialogFragment = new ConfigurationDialogFragment();
        mDialogFragment.show(getSupportFragmentManager(), TAG);
        return true;
    }

    /**
     * Start voice operation to translate speech-to-text.
     *
     * @param view
     */
    @OnClick(R.id.main_image_button_speech)
    public void startSpeech(View view) {
        rippleBackground.startRippleAnimation();
        startVoiceOperation(view);
    }

    /**
     * initialize a RecyclerView
     */
    private void setUpRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mReservationAdapter = new ReservationAdapter(mReservationList);
        mRecyclerView.setAdapter(mReservationAdapter);
    }


    /**
     * Set speech to text preferences.
     *
     * @param speechPreferences show microphone.
     */
    private void setSpeechPreferences(Boolean speechPreferences) {
        if (speechPreferences) {
            mImageButtonSpeech.setVisibility(View.VISIBLE);
            if (mThemeIsDark) {
                mImageButtonSpeech.setImageResource(R.drawable.microphone_dark);
            } else {
                mImageButtonSpeech.setImageResource(R.drawable.microphone_light);
            }
        } else {
            mImageButtonSpeech.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Set the correct status.
     *
     * @param status which text must be showed,
     * @param color  in which color must the text be.
     */
    private void setAvailability(int status, int color) {
        mTextViewAvailability.setText(mResources.getStringArray(R.array.main_availability)[status]);
        mTextViewCheckInStatus.setText(mResources.getStringArray(R.array.main_check_in_status)[status]);
        mTextViewAvailability.setTextColor(mResources.getColor(color));
    }

    /**
     * show the layout for check in message.
     */
    private void showCheckInMessage() {
        mConstraintLayoutData.setVisibility(View.INVISIBLE);
        mConstraintLayoutNotifications.setVisibility(View.VISIBLE);
    }

    /**
     * show the layout for reservation.
     */
    private void showReservation() {
        mConstraintLayoutReservations.setVisibility(View.VISIBLE);
        mConstraintLayoutErrorMessage.setVisibility(View.INVISIBLE);
    }

    /**
     * handles check in events.
     */
    private void checkIn() {
        setAvailability(STATUS_OCCUPIED, R.color.occupied_color);
        setRelativeCheckInMessage();
        showCheckInMessage();
        changeDeviceColor(true);
        mCheckedInStatus = true;
        saveStatusPreference(mCheckedInStatus);
    }

    /**
     * handles check out events.
     */
    private void checkOut() {
        setAvailability(STATUS_VACANT, R.color.vacant_color);
        setRelativeCheckInMessage();
        showCheckInMessage();
        changeDeviceColor(false);
        mCheckedInStatus = false;
        saveStatusPreference(mCheckedInStatus);
    }

    /**
     * save preference for the status
     *
     * @param status in boolean
     */
    private void saveStatusPreference(boolean status) {
        mEditor.putBoolean("systemStatus", status);
        mEditor.apply();
    }

    /**
     * show corresponding message according to error.
     *
     * @param message               to show.
     * @param isReservableUnitError check if device exist.
     */
    private void showRelativeErrorMessage(String message, boolean isReservableUnitError) {
        mConstraintLayoutReservations.setVisibility(View.INVISIBLE);
        mConstraintLayoutErrorMessage.setVisibility(View.VISIBLE);
        mTextViewErrorText.setText(message);

        if (isReservableUnitError) {
            mTextViewReservableUnitName.setVisibility(View.INVISIBLE);
            mTextViewAvailability.setVisibility(View.INVISIBLE);
            mTextViewCheckInStatus.setVisibility(View.INVISIBLE);
            mImageButtonSpeech.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleNfcIntent(intent);
    }

    /**
     * handleSyncOnUpdate the NFC intent
     *
     * @param intent pass intent
     */
    private void handleNfcIntent(Intent intent) {
        String newTag = NfcHandler.getTagFromNfcIntent(intent);
        ApiCallHandler.getInstance().getMeetingStatus(this, this, newTag);
    }

    /**
     * check if user can check in and call corresponding event/s.
     */
    private void onCheckInStatusSet() {
        if (mNfcHandler.getCheckInStatus() != null) {
            CheckInStatus currentCheckInStatus = mNfcHandler.getCheckInStatus();

            if (currentCheckInStatus.isUnitOccupied()) {
                checkIn();
            } else if (currentCheckInStatus.isReservationAvailable()) {
                showCheckInMessage();
                setRelativeCheckInMessage();
            } else {
                checkOut();
            }
        }

        showDataAfterDelay();
    }

    /**
     * show corresponding message according to check in responded status.
     */
    private void setRelativeCheckInMessage() {
        switch (mNfcHandler.getCheckInStatus().getStatus()) {
            case CheckInStatus.MEETING_SET_TO_IN_PROGRESS:
                mTextViewNotification.setText(String.format(mResources.getStringArray(R.array.main_checked_in_message)[0], Utils.getRelativeGreeting(mResources), mNfcHandler.getCheckInStatus().getReserveeInternal()));
                break;
            case CheckInStatus.MEETING_WAS_FINISHED_BUT_NOW_SET_TO_IN_PROGRESS:
                mTextViewNotification.setText(String.format(mResources.getStringArray(R.array.main_checked_in_message)[0], Utils.getRelativeGreeting(mResources), mNfcHandler.getCheckInStatus().getReserveeInternal()));
                break;
            case CheckInStatus.MEETING_SET_TO_IN_COMPLETED:
                mTextViewNotification.setText(mResources.getStringArray(R.array.main_checked_in_message)[1]);
                break;
            case CheckInStatus.COULD_NOT_SET_MEETING_STATUS:
                mTextViewNotification.setText(String.format(mResources.getStringArray(R.array.main_checked_in_message)[6],
                        Utils.getRelativeGreeting(mResources), mNfcHandler.getCheckInStatus().getReserveeInternal()));
                break;
            case CheckInStatus.RESERVATION_ACTIVE_AFTER_SECONDS:
                float time = Float.valueOf(mNfcHandler.getCheckInStatus().getDuration());
                float differenceInMinute = Utils.getRelativeTimeFromSeconds(Utils.TYPE_TIME_MINUTES, time);

                if (differenceInMinute > 60) {
                    mTextViewNotification.setText(String.format(mResources.getStringArray(R.array.main_checked_in_message)[4],
                            Utils.getRelativeGreeting(mResources), mNfcHandler.getCheckInStatus().getReserveeInternal(),
                            (int) (Utils.getRelativeTimeFromSeconds(Utils.TYPE_TIME_HOUR, time)),
                            Math.round(Utils.getRelativeTimeFromSeconds(Utils.TYPE_REST_MINUTES, time))));
                } else if (differenceInMinute < 60 && differenceInMinute > 1) {
                    mTextViewNotification.setText(String.format(mResources.getStringArray(R.array.main_checked_in_message)[3],
                            Utils.getRelativeGreeting(mResources), mNfcHandler.getCheckInStatus().getReserveeInternal(),
                            Math.round(Utils.getRelativeTimeFromSeconds(Utils.TYPE_TIME_MINUTES, time))));
                } else {
                    mTextViewNotification.setText(String.format(mResources.getStringArray(R.array.main_checked_in_message)[7],
                            Utils.getRelativeGreeting(mResources), mNfcHandler.getCheckInStatus().getReserveeInternal(),
                            ((int) time)));
                }
                break;
            case CheckInStatus.USER_NOT_FOUND:
                mTextViewNotification.setText(mResources.getStringArray(R.array.main_checked_in_message)[5]);
                break;
            case CheckInStatus.NO_ACTIVE_RESERVATION_FOUND:
                mTextViewNotification.setText(mResources.getStringArray(R.array.main_checked_in_message)[2]);
                break;
            case CheckInStatus.MEETING_SET_TO_IN_COMPLETED_AND_ENDED_THE_MEETING:
                mEditor.putString("reservationEndTime", Utils.currentDayTime());
                mEditor.apply();
                break;
        }
    }

    /**
     * get reservations.
     */
    public void getReservations() {
        if (Utils.hasInternetConnection(this)) {
            ApiCallHandler.getInstance().getReservations(mSystemId, this); //get reservation from api
        } else {
            showRelativeErrorMessage(getString(R.string.no_internet_connection), false);
        }
    }

    /**
     * show data after a delay
     */
    private void showDataAfterDelay() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mConstraintLayoutData.setVisibility(View.VISIBLE);
                mConstraintLayoutNotifications.setVisibility(View.INVISIBLE);
            }
        }, DELAY_SHOW_DATA);
    }

    /**
     * update current state
     * get reservation (If preference is false)
     *
     * @return a runnable
     */
    private Runnable runnableCheckCurrentState() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (!mSyncOnUpdate) {
                    getReservations();
                }

                updateCurrentState();
                handleSyncOnUpdate();
            }
        };

        return runnable;
    }

    /**
     * Start runnable
     */
    private void handleSyncOnUpdate() {
        mCheckinStatusHandler.postDelayed(runnableCheckCurrentState(), DELAY_MINUTE);
    }

    /**
     * change UI according to current state
     */
    public void updateCurrentState() {
        if (mCurrentReservation != null) {
            try {
                comparedTimeDifferent = Utils.compareDate(mCurrentReservation);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (!mCheckedInStatus || (comparedTimeDifferent >= 0 && mCheckedInStatus)) {
                setAvailability(STATUS_VACANT, R.color.vacant_color);
                changeDeviceColor(false);
                mCheckedInStatus = false;
                saveStatusPreference(mCheckedInStatus);
            } else {
                setAvailability(STATUS_OCCUPIED, R.color.occupied_color);
                changeDeviceColor(true);
            }
        } else {
            setAvailability(STATUS_VACANT, R.color.vacant_color);
            changeDeviceColor(false);
        }
    }

    /**
     * changed device color
     *
     * @param occupied check if device is occupied
     */
    private void changeDeviceColor(boolean occupied) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (occupied) {
            DeviceLedHandler.setFrontLedColor(DeviceLedHandler.COLOR_OCCUPIED, this);
        } else {
            DeviceLedHandler.setFrontLedColor(DeviceLedHandler.COLOR_VACANT, this);
        }
    }

    /**
     * get list of result
     *
     * @param results      arraylist of result
     * @param responseType the type of response
     */
    @Override
    public void onCallSuccessListResult(ArrayList results, int responseType) {
        switch (responseType) {
            case RESPONSE_TYPE_RESERVATIONS:
                if (results != null && !results.isEmpty()) {
                    mReservationList = new ArrayList<>();
                    mReservationList = results;
                    Collections.sort(mReservationList);

                    String systemReservation = mReservationList.get(0).getEndAt().getDisplayValue();

                    SharedPreferences prefs = getSharedPreferences(NAME_SHARED_PREFERENCES, MODE_PRIVATE);

                    mCurrentReservation = prefs.getString("reservationEndTime", null);

                    if (mCurrentReservation == null || !(mCurrentReservation.equals(systemReservation))) {
                        mEditor.putString("reservationEndTime", systemReservation);
                        mEditor.apply();
                    }

                    setUpRecyclerView();
                    showReservation();
                } else {
                    showRelativeErrorMessage(mResources.getString(R.string.no_reservations_found), false);
                }
                break;
            case RESPONSE_TYPE_RESERVABLE_UNIT:
                if (results != null && !results.isEmpty()) {
                    ReservableUnit ru = (ReservableUnit) results.get(0);
                    UnitManager.getInstance().setReservableUnit(ru);

                    mEditor.putString("systemId", ru.getSystemId());
                    mEditor.putString("systemName", ru.getName());
                    mEditor.apply();

                    new RegisterForPushNotificationsAsync().execute();
                    getReservations();
                    mTextViewReservableUnitName.setText(ru.getName());

                } else {
                    showRelativeErrorMessage(mResources.getString(R.string.unknown_device), false);
                }
                break;
        }
    }

    /**
     * get object of result on successful call
     *
     * @param object       object of result
     * @param responseType the type of response
     */
    @Override
    public void onCallSuccessObjectResult(Object object, int responseType) {

        switch (responseType) {
            case RESPONSE_TYPE_CHECKIN_STATUS:
                CheckInStatus checkInStatus = (CheckInStatus) object;
                mNfcHandler.setmCheckInStatus(checkInStatus);
                onCheckInStatusSet();
                break;
        }
    }

    /**
     * the call has failed
     *
     * @param responseType the type of response
     */
    @Override
    public void OnCallFailed(int responseType) {
        switch (responseType) {
            case RESPONSE_TYPE_RESERVATIONS:
                showRelativeErrorMessage(mResources.getString(R.string.no_reservations_found), false);
                break;
            case RESPONSE_TYPE_RESERVABLE_UNIT:
                showRelativeErrorMessage(mResources.getString(R.string.unverified_reservable_unit), false);
                break;
        }
    }

    /**
     *
     */
    public enum FinalResponseStatus {
        NotReceived, OK, Timeout
    }


    /**
     * @return
     */

    public String getPrimaryKey() {
        return this.getString(R.string.primaryKey);
    }

    /**
     * @return
     */
    private Boolean getUseMicrophone() {
        return true;
    }

    /**
     * @return
     */
    private SpeechRecognitionMode getMode() {
        return SpeechRecognitionMode.ShortPhrase;
    }

    /**
     * @return
     */
    private String getDefaultLocale() {
        return "en-us";
    }

    /**
     * @return
     */
    private String getAuthenticationUri() {
        return this.getString(R.string.authenticationUri);
    }


    /**
     * Handles the Click event of the _startButton control.
     */
    private void startVoiceOperation(View arg0) {
        this.m_waitSeconds = this.getMode() == SpeechRecognitionMode.ShortPhrase ? 20 : 200;

        Log.d("Start speech", "--- Start speech recognition ---");
        if (this.getUseMicrophone()) {
            if (this.micClient == null) {


                this.micClient = SpeechRecognitionServiceFactory.createMicrophoneClient(
                        this,
                        this.getMode(),
                        this.getDefaultLocale(),
                        this,
                        this.getPrimaryKey());

                this.micClient.setAuthenticationUri(this.getAuthenticationUri());
            }

            this.micClient.startMicAndRecognition();
        } else {
            if (null == this.dataClient) {

                this.dataClient = SpeechRecognitionServiceFactory.createDataClient(
                        this,
                        this.getMode(),
                        this.getDefaultLocale(),
                        this,
                        this.getPrimaryKey());

                this.dataClient.setAuthenticationUri(this.getAuthenticationUri());
            }

        }
    }


    @Override
    public void onPartialResponseReceived(String s) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onFinalResponseReceived(RecognitionResult recognitionResult) {
        //Received Response
        boolean isFinalDicationMessage = this.getMode() == SpeechRecognitionMode.LongDictation &&
                (recognitionResult.RecognitionStatus == RecognitionStatus.EndOfDictation ||
                        recognitionResult.RecognitionStatus == RecognitionStatus.DictationEndSilenceTimeout);
        if (null != this.micClient && this.getUseMicrophone() && ((this.getMode() == SpeechRecognitionMode.ShortPhrase) || isFinalDicationMessage)) {
            // we got the final result, so it we can end the mic reco.  No need to do this
            // for dataReco, since we already called endAudio() on it as soon as we were done
            // sending all the data.
            this.micClient.endMicAndRecognition();
        }

        if (isFinalDicationMessage) {
            //this._startButton.setEnabled(true);
            this.isReceivedResponse = FinalResponseStatus.OK;
            Log.d("final", "final" + isReceivedResponse);
        }

        if (!isFinalDicationMessage) {
            Log.d("Final n-BEST Results ", "");
            for (int i = 0; i < recognitionResult.Results.length; i++) {
                Log.d("[" + i + "] Confidence", " " + recognitionResult.Results[i].Confidence +
                        " Text=\"" + recognitionResult.Results[i].DisplayText);
            }
            //Log.d("!final", " " + recognitionResult.Results[0].DisplayText);

            //Stop animation
            rippleBackground.stopRippleAnimation();

//


            if (recognitionResult.Results.length > 0) {
                //Show Popup
                listeningDialog.showDialog(this, recognitionResult.Results[0].DisplayText);

                //Here is the request that has to be made to ServiceNow
                try {
                    ApiCallHandler.getInstance().postVoiceTranslation(mSystemId, recognitionResult.Results[0].DisplayText);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Post API", "Could not start post request");
                }
            }
        }
    }

    @Override
    public void onIntentReceived(String s) {

    }

    @Override
    public void onError(int i, String s) {
        //this._startButton.setEnabled(true);
        Log.d("", "--- Error received by onError() ---");
        Log.d("Error code: ", String.valueOf(SpeechClientStatus.fromInt(i)) + " " + String.valueOf(i));
        Log.d("Error Text", s);
        rippleBackground.stopRippleAnimation();
    }

    @Override
    public void onAudioEvent(boolean b) {
        Log.d("Mic status: ", "sup" + b);

        if (b) {
            Log.d("Mic is on", "Please start speaking");
        }

        if (!b) {
            micClient.endMicAndRecognition();
        }
    }

    private class RecognitionTask extends AsyncTask<Void, Void, Void> {
        DataRecognitionClient dataClient;
        SpeechRecognitionMode recoMode;
        String filename;

        RecognitionTask(DataRecognitionClient dataClient, SpeechRecognitionMode recoMode, String filename) {
            this.dataClient = dataClient;
            this.recoMode = recoMode;
            this.filename = filename;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // Note for wave files, we can just send data from the file right to the server.
                // In the case you are not an audio file in wave format, and instead you have just
                // raw data (for example audio coming over bluetooth), then before sending up any
                // audio data, you must first send up an SpeechAudioFormat descriptor to describe
                // the layout and format of your raw audio data via DataRecognitionClient's sendAudioFormat() method.
                // String filename = recoMode == SpeechRecognitionMode.ShortPhrase ? "whatstheweatherlike.wav" : "batman.wav";
                InputStream fileStream = getAssets().open(filename);
                int bytesRead = 0;
                byte[] buffer = new byte[1024];

                do {
                    // Get  Audio data to send into byte buffer.
                    bytesRead = fileStream.read(buffer);

                    if (bytesRead > -1) {
                        // Send of audio data to service.
                        dataClient.sendAudio(buffer, bytesRead);
                    }
                } while (bytesRead > 0);

            } catch (Throwable throwable) {
                throwable.printStackTrace();
            } finally {
                dataClient.endAudio();
            }

            return null;
        }
    }

    /**
     * Register device in pushy and save token in servicenow
     */
    private class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception> {
        protected Exception doInBackground(Void... params) {
            try {
                // Assign a unique token to this device
                String deviceToken = Pushy.register(getApplicationContext());

                // Send the token to your backend server via an HTTP GET request
                ApiCallHandler.getInstance().registerPushyToken(mSystemId, deviceToken);
            } catch (Exception exc) {
                // Return exc to onPostExecute
                return exc;
            }

            // Success
            return null;
        }

        @Override
        protected void onPostExecute(Exception exc) {
            // Failed?
            if (exc != null) {
                // Show error as toast message
                Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_LONG).show();
            }

            // Succeeded, do something to alert the user
        }
    }

}



