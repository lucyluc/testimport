package activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.plat4mation.project4u.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import handlers.ApiCallHandler;
import interfaces.ResponseCallBack;
import models.InstanceProperty;
import util.Utils;

import static util.Utils.getMacAddress;

public class LoginActivity extends AppCompatActivity implements ResponseCallBack {
    private static final String TAG = "LoginActivity";

    private static final boolean NO_INTERNET_CONNECTION = false;
    private static final boolean INTERNET_CONNECTION = true;
    private static final int CHECK_INTERNET_CONNECTION_AFTER = 300;

    @BindView(R.id.login_edit_text_url) TextView mEditTextUrl;
    @BindView(R.id.login_edit_text_username) TextView mEditTextUsername;
    @BindView(R.id.login_edit_text_password) TextView mEditTextPassword;
    @BindView(R.id.login_edit_text_mac_address) TextView mEditTextMacAddress;
    @BindView(R.id.login_layout_main) ConstraintLayout mLayoutMain;
    @BindView(R.id.login_layout_error_message) ConstraintLayout mLayoutErrorMessage;

    private ApiCallHandler mApiCallHandler;
    private SharedPreferences mSharedPreferences;
    private boolean mProvidedNewCredentials;
    private Handler mCheckInternetStatusHandler;
    private SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences = getSharedPreferences(MainActivity.NAME_SHARED_PREFERENCES, MODE_PRIVATE);
        Utils.setThemePreferences(this, mSharedPreferences.getBoolean("enable_dark_theme", true));

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        //check internet connection
        if (!(Utils.hasInternetConnection(this))){
            changeLayout(NO_INTERNET_CONNECTION);
            mCheckInternetStatusHandler = new Handler();
            handleCheckInternetState();
            return;
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Utils.hideSystemUI(this);

        mApiCallHandler = ApiCallHandler.getInstance();
        mEditTextMacAddress.setText(getMacAddress(getApplicationContext()));

        //check if user come back from main activity
        if (getIntent().hasExtra("Configuration") && (getIntent().getExtras()).getBoolean("Configuration")){
            mEditTextUrl.setHint(Utils.decrypt(mSharedPreferences.getString("key_instance", null)));
            mEditTextUsername.setHint(Utils.decrypt(mSharedPreferences.getString("key_username", null)));
            mEditTextPassword.setHint("***********");
            getPreferences();
            return;
        }

        //check if preferences exists
        if (mSharedPreferences.contains("key_instance") &&
                mSharedPreferences.contains("key_username") &&
                mSharedPreferences.contains("key_password")) {

            mApiCallHandler.setInstance(Utils.decrypt(mSharedPreferences.getString("key_instance", null)));
            mApiCallHandler.setBasicAuthCredentials(Utils.decrypt(mSharedPreferences.getString("key_username", null)),
                    Utils.decrypt(mSharedPreferences.getString("key_password", null)));
            mApiCallHandler.authenticate(this);
        }
    }

    /**
     * cancel the configuration
     */
    @OnClick(R.id.login_button_cancel)
    public void cancelConfiguration(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * minimize the application
     */
    @OnClick(R.id.login_button_close)
    public void minimizeApplication(){
        finishAffinity();
    }

    /**
     * Login on the instance
     */
    @OnClick(R.id.login_button_send)
    public void authenticate() {
        String url = mEditTextUrl.getText().toString();
        String username = mEditTextUsername.getText().toString();
        String password = mEditTextPassword.getText().toString();

        if (!url.isEmpty() && !username.isEmpty() && !password.isEmpty()) {
            mProvidedNewCredentials = true;
            mApiCallHandler.setInstance(url);
            mApiCallHandler.setBasicAuthCredentials(username, password);
            mApiCallHandler.authenticate(this);
            return;
        }

        if (url.isEmpty()){
            mEditTextUrl.setError(getString(R.string.login_no_url_filled));
        }

        if (username.isEmpty()){
            mEditTextUsername.setError(getString(R.string.login_no_username_filled));
        }

        if (password.isEmpty()){
            mEditTextPassword.setError(getString(R.string.login_no_password_filled));
        }
    }

    /**
     * reconnect to the internet
     */
    @OnClick(R.id.login_button_reconnect)
    public void reconnect(){
        if (Utils.hasInternetConnection(this)){
            changeLayout(INTERNET_CONNECTION);
        }
    }

    @Override
    public void onCallSuccessListResult(ArrayList result, int responseType) {}

    /**
     * get object of result on successful call
     *
     * @param object object of result
     * @param responseType the type of response
     */
    @Override
    public void onCallSuccessObjectResult(Object object, int responseType) {
        mEditor = getSharedPreferences(MainActivity.NAME_SHARED_PREFERENCES, MODE_PRIVATE).edit();
        switch (responseType) {
            case RESPONSE_TYPE_AUTHENTICATE:
                if (mProvidedNewCredentials) {
                    getSharedPreferences(MainActivity.NAME_SHARED_PREFERENCES, MODE_PRIVATE)
                            .edit().putString("key_instance", Utils.encrypt(mEditTextUrl.getText().toString())).apply();
                    getSharedPreferences(MainActivity.NAME_SHARED_PREFERENCES, MODE_PRIVATE)
                            .edit().putString("key_username", Utils.encrypt(mEditTextUsername.getText().toString())).apply();
                    getSharedPreferences(MainActivity.NAME_SHARED_PREFERENCES, MODE_PRIVATE)
                            .edit().putString("key_password", Utils.encrypt(mEditTextPassword.getText().toString())).apply();
                }

                getPreferences();
                navigateToMainActivity();
                break;
            case RESPONSE_TYPE_INSTANCE_PROPERTY_DARK_THEME:
                if (mSharedPreferences.getBoolean("enable_dark_theme", true) != ((InstanceProperty) object).isDarkTheme()){
                    mEditor.putBoolean("enable_dark_theme", ((InstanceProperty) object).isDarkTheme());
                    mEditor.apply();
                    LoginActivity.this.recreate();
                }

                break;
            case RESPONSE_TYPE_INSTANCE_PROPERTY_SPEECH_TO_TEXT:
                mEditor.putBoolean("enable_speech_to_text", ((InstanceProperty) object).isSpeechToText());
                mEditor.apply();
                break;
            case RESPONSE_TYPE_INSTANCE_PROPERTY_SYNC_ON_UPDATE:
                mEditor.putBoolean("enable_sync_on_update", ((InstanceProperty) object).isSyncOnUpdate());
                mEditor.apply();
                break;
        }
    }

    /**
     * the call has failed
     *
     * @param responseType the type of response
     */
    @Override
    public void OnCallFailed(int responseType) {
        switch (responseType){
            case 203:
                Toast.makeText(this, "203 Non-Authoritative Information", Toast.LENGTH_LONG).show();
                break;
            case 301:
                Toast.makeText(this, "301 Moved Permanently", Toast.LENGTH_LONG).show();
                break;
            case 400:
                Toast.makeText(this, "400 Bad request", Toast.LENGTH_LONG).show();
                break;
            case 401:
                Toast.makeText(this, "401 Unauthorized", Toast.LENGTH_LONG).show();
                mEditTextUsername.setError(getString(R.string.login_error_message_wrong_credentials));
                mEditTextPassword.setError(getString(R.string.login_error_message_wrong_credentials));
                break;
            case 403:
                Toast.makeText(this, "403 Forbidden", Toast.LENGTH_LONG).show();
                break;
            case 404:
                Toast.makeText(this, "404 Not Found", Toast.LENGTH_LONG).show();
                break;
            default:
                Toast.makeText(this, "Something went wrong!", Toast.LENGTH_LONG).show();

        }
    }

    /**
     * navigate to the main activity
     */
    private void navigateToMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * change layout according to internet connection
     * @param isConnected in boolean
     */
    public void changeLayout(Boolean isConnected){
        mLayoutMain.setVisibility(isConnected ? View.VISIBLE : View.INVISIBLE);
        mLayoutErrorMessage.setVisibility(isConnected ? View.INVISIBLE : View.VISIBLE);
    }

    /**
     * get instance property from service now
     */
    private void getPreferences(){
        mApiCallHandler.getInstanceProperty(this, ApiCallHandler.DARK_THEME);
        mApiCallHandler.getInstanceProperty(this, ApiCallHandler.SPEECH_TO_TEXT);
        mApiCallHandler.getInstanceProperty(this, ApiCallHandler.SYNC_ON_UPDATE);
    }


    private Runnable runnableCheckCurrentInternetState(final Context context) {
        Runnable runnable =  new Runnable() {
            @Override
            public void run() {
                if (Utils.hasInternetConnection(context)){
                    LoginActivity.this.recreate();
                }

                handleCheckInternetState();
            }
        };

        return runnable;
    }

    private void handleCheckInternetState() {
        mCheckInternetStatusHandler.postDelayed(runnableCheckCurrentInternetState(this), CHECK_INTERNET_CONNECTION_AFTER);
    }


}
