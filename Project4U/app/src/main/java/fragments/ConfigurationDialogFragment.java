package fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.plat4mation.project4u.R;

import java.util.ArrayList;

import activities.LoginActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import handlers.ApiCallHandler;
import interfaces.ResponseCallBack;
import models.InstanceProperty;

public class ConfigurationDialogFragment extends AppCompatDialogFragment implements ResponseCallBack {

    @BindView(R.id.dialog_edit_text_password) EditText mInputCode;
    private String mCode;

    public ConfigurationDialogFragment() {
        ApiCallHandler.getInstance().getInstanceProperty(this, ApiCallHandler.LOGIN_CODE);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_configuration, null);
        ButterKnife.bind(this, view);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(view)
                .setPositiveButton(R.string.check, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        if (!mInputCode.getText().toString().equals("") || !mInputCode.getText().toString().isEmpty()){
                            String currentCode = mInputCode.getText().toString();
                            if (currentCode.equals(mCode)){
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                intent.putExtra("Configuration", true);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getActivity(), R.string.configuration_mismatch, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), R.string.login_no_password_filled, Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ConfigurationDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Override
    public void onCallSuccessListResult(ArrayList result, int responseType) {

    }

    @Override
    public void onCallSuccessObjectResult(Object object, int responseType) {
        mCode = ((InstanceProperty) object).getLoginCode();
    }

    @Override
    public void OnCallFailed(int responseType) {

    }
}