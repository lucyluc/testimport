package dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.plat4mation.project4u.R;

/**
 * Created by Aschwin Bruyning on 12-6-2018.
 */

public class ViewListeningDialog {
    TextView translationText;

    public void showDialog(final Activity activity, final String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.activity_listening_dialog);

        translationText = dialog.findViewById(R.id.text_dialog);
        translationText.setText(msg);


        final Button dialogButton = (Button) dialog.findViewById(R.id.btn_voice_close);
        dialogButton.setVisibility(View.VISIBLE);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
