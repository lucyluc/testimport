package handlers;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import javax.net.ssl.SSLHandshakeException;

import util.Utils;

public class DeviceLedHandler {
    private static final String TAG = "DeviceLedHandler";

    public static final String URL_PRE_FIXED = "http://";
    public static final String URL_POST_FIXED = ":8080/v1/led/front_led";
    public static final String STR_METHOD_POST = "POST";

    public static final Color COLOR_OCCUPIED = new Color(255, 0, 0);
    public static final Color COLOR_VACANT = new Color(0, 255, 0);

    public DeviceLedHandler() {
    }

    /**
     * set device led light color
     * @param newColor color to set
     * @param context to get ip address
     * @return true for success and false for failure
     */
    public static boolean setFrontLedColor(Color newColor, Context context) {
        boolean ret = false;
        JSONObject aObj = new JSONObject();
        HttpURLConnection conn = null;
        try {
            aObj.put("red", Integer.toString(newColor.red));
            aObj.put("green", Integer.toString(newColor.green));
            aObj.put("blue", Integer.toString(newColor.blue));
            URL url = new URL(URL_PRE_FIXED + Utils.getIp(context) + URL_POST_FIXED);
            conn = (HttpURLConnection) url.openConnection();
            if (null != conn) {
                conn.setConnectTimeout(5000);
                conn.setReadTimeout(5000);
                conn.setRequestMethod(STR_METHOD_POST);
                conn.setRequestProperty("Content-Type", "application/json");
                OutputStream os = conn.getOutputStream();
                os.write(aObj.toString().getBytes("UTF-8"));
                os.close();
                int responseCode = conn.getResponseCode();
                BufferedReader in = null;
                switch (responseCode) {
                    case HttpURLConnection.HTTP_ACCEPTED:
                    case HttpURLConnection.HTTP_OK:
                        ret = true;
                        in = new BufferedReader(
                                new InputStreamReader(conn.getInputStream()));
                        break;

                    default:
                        in = new BufferedReader(
                                new InputStreamReader(conn.getErrorStream()));
                        break;
                }

                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            }
        } catch (JSONException e) {
            Log.e(TAG, "sendRestfulApiCmd JSONException", e);
        } catch (SSLHandshakeException e) {
            Log.e(TAG, "sendRestfulApiCmd, SSLHandshakeException", e);
        } catch (UnknownHostException e) {
            Log.e(TAG, "sendRestfulApiCmd, UnknownHostException", e);
        } catch (MalformedURLException e) {
            Log.e(TAG, "sendRestfulApiCmd, MalformedURLException", e);
        } catch (IOException e) {
            Log.e(TAG, "sendRestfulApiCmd, IOException", e);
        } catch (Exception e) {
            Log.e(TAG, "sendRestfulApiCmd, Exception", e);
        } finally {
            if (null != conn) {
                conn.disconnect();
                conn = null;
            }
        }
        return ret;
    }

    public static class Color {
        int red;
        int green;
        int blue;

        public Color(int red, int green, int blue) {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }
    }
}
