package handlers;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;

import models.CheckInStatus;

public class NfcHandler {

    private static NfcHandler sInstance;
    private CheckInStatus mCheckInStatus;

    public static NfcHandler getInstance() {
        if(sInstance == null) {
            sInstance = new NfcHandler();
        }

        return sInstance;
    }

    public void setmCheckInStatus(CheckInStatus mCheckInStatus) {
        this.mCheckInStatus = mCheckInStatus;
    }

    public CheckInStatus getCheckInStatus() {
        return mCheckInStatus;
    }

    public static String getTagFromNfcIntent(Intent intent) {
        Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        byte[] extraID = tagFromIntent.getId();

        StringBuilder sb = new StringBuilder();

        for (byte b : extraID) {
            sb.append(String.format("%02X", b));
        }

        return sb.toString();
    }

}
