package handlers;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.ArrayMap;
import android.util.Log;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import controls.BasicAuthInterceptor;
import interfaces.ResponseCallBack;
import interfaces.RetrofitApiCall;
import models.CheckInStatus;
import models.CheckInStatusResult;
import models.InstanceProperty;
import models.ReservableUnitResult;
import models.ReservationResult;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import util.Preferences;
import util.Utils;

public class ApiCallHandler {

    private static final String TAG = "ApiCallHandler";

    private final String BASE_URL_HTTP = "https://";
    private final String BASE_URL_END_POINT = ".service-now.com/api/";
    private final String AUTH_URL = "now/table/x_pl4_facility4u_reservable?sysparm_fields=number&sysparm_limit=1";
    private final String RESERVABLE_UNIT_URL = "now/table/x_pl4_facility4u_reservable?&sysparm_fields=sys_id%2Cname&sysparm_limit=1&sysparm_query=technical_id%3D";
    private final String RESERVATION_URL = "now/table/x_pl4_facility4u_reservation?sysparm_display_value=all" +
            "&sysparm_exclude_reference_link=true" +
            "&sysparm_query=start_atRELATIVEGE%40hour%40ago%4010%5Eend_atRELATIVEGE%40minute%40ago%400%5EstatusNOT%20INcancelled%2Crejected%5Ereservable%3D";
    private final String MEETING_STATUS_URL_TECHNICAL_USER_ID = "x_pl4_facility4u/reservation/status_update?technical_user_id=";
    private final String MEETING_STATUS_URL_TECHNICAL_DEVICE_ID = "&technical_device_id=";
    private final String PROPERTY_URL = "x_pl4_facility4u/generic/get_property/";
    private final String PUSHY_TOKEN_URL = "now/table/x_pl4_facility4u_reservable/";
    public static final String DARK_THEME = "desk_device_enable_dark_theme";
    public static final String SPEECH_TO_TEXT = "desk_device_enable_speech_to_text";
    public static final String SYNC_ON_UPDATE = "desk_device_enable_sync_on_update";
    public static final String LOGIN_CODE = "desk_device_login_code";
    private final String TEXT_TO_SPEECH_SAVE_URL = "x_pl4_facility4u/generic/desk_device_text_to_speech/";

    private BasicAuthInterceptor mBasicAuthInterceptor;
    private String mInstance;
    private static ApiCallHandler mHandlerInstance;

    public static ApiCallHandler getInstance() {
        if (mHandlerInstance == null) {
            mHandlerInstance = new ApiCallHandler();
        }

        return mHandlerInstance;
    }

    /**
     * setup retrofitApiCall
     *
     * @param baseUrl pass the base url
     * @return the retrofitApiCall
     */
    public RetrofitApiCall RetrofitApiCall(String baseUrl) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(mBasicAuthInterceptor)
                .build();
        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(RetrofitApiCall.class);
    }

    /**
     * get reservable unit api call
     *
     * @param context          pass context to get mac address
     * @param responseCallBack to notify failure or success
     */
    public void getReservableUnit(final Context context, final ResponseCallBack responseCallBack) {
        String macAddress = Utils.getMacAddress(context);
        String url = RESERVABLE_UNIT_URL + macAddress;

        Call<ReservableUnitResult> callReservableUnit = RetrofitApiCall(BASE_URL_HTTP + mInstance + BASE_URL_END_POINT).getReservableUnit(url);
        callReservableUnit.enqueue(new Callback<ReservableUnitResult>() {
            @Override
            public void onResponse(Call<ReservableUnitResult> call, Response<ReservableUnitResult> response) {
                ArrayList result = new ArrayList();

                if (response.body() != null && !response.body().getResults().isEmpty()) {
                    result.add(response.body().getResults().get(0));
                }

                responseCallBack.onCallSuccessListResult(result, ResponseCallBack.RESPONSE_TYPE_RESERVABLE_UNIT);
            }

            @Override
            public void onFailure(Call<ReservableUnitResult> call, Throwable t) {
                responseCallBack.OnCallFailed(ResponseCallBack.RESPONSE_TYPE_RESERVABLE_UNIT);
            }
        });
    }

    /**
     * get reservation api call
     *
     * @param systemId         pass the systemID
     * @param responseCallBack to notify failure or success
     */
    public void getReservations(String systemId, final ResponseCallBack responseCallBack) {
        String url = RESERVATION_URL + systemId;

        Call<ReservationResult> callReservation = RetrofitApiCall(BASE_URL_HTTP + mInstance + BASE_URL_END_POINT).getAllReservations(url);
        callReservation.enqueue(new Callback<ReservationResult>() {
            @Override
            public void onResponse(Call<ReservationResult> call, Response<ReservationResult> response) {
                if (response.body() == null) {
                    responseCallBack.onCallSuccessListResult(new ArrayList(), ResponseCallBack.RESPONSE_TYPE_RESERVATIONS);
                } else {
                    responseCallBack.onCallSuccessListResult(response.body().getReservations(), ResponseCallBack.RESPONSE_TYPE_RESERVATIONS);
                }
            }

            @Override
            public void onFailure(Call<ReservationResult> call, Throwable t) {
                responseCallBack.OnCallFailed(ResponseCallBack.RESPONSE_TYPE_RESERVATIONS);
            }
        });
    }

    /**
     * get Meeting Status api call
     *
     * @param context          to get mac address
     * @param responseCallBack to notify failure or success
     * @param nfcTag           pass the nfc tag
     */
    public void getMeetingStatus(final Context context, final ResponseCallBack responseCallBack, String nfcTag) {
        String url = MEETING_STATUS_URL_TECHNICAL_USER_ID + nfcTag +
                MEETING_STATUS_URL_TECHNICAL_DEVICE_ID + Utils.getMacAddress(context);

        Call<CheckInStatusResult> callMeetingStatus = RetrofitApiCall(BASE_URL_HTTP + mInstance + BASE_URL_END_POINT).checkMeetingStatus(url);
        callMeetingStatus.enqueue(new Callback<CheckInStatusResult>() {
            @Override
            public void onResponse(Call<CheckInStatusResult> call, Response<CheckInStatusResult> response) {
                CheckInStatus status = new CheckInStatus();
                status.setDuration(response.body().getResponseResult().getDuration());
                status.setReserveeInternal(response.body().getResponseResult().getReserveeInternal());
                status.setStatus(response.body().getResponseResult().getStatus());

                responseCallBack.onCallSuccessObjectResult(status, ResponseCallBack.RESPONSE_TYPE_CHECKIN_STATUS);
            }

            @Override
            public void onFailure(Call<CheckInStatusResult> call, Throwable t) {

            }
        });
    }


    /**
     * get mInstance property api call
     *
     * @param responseCallBack to notify failure or success
     * @param propertyType     to get given property
     */
    public void getInstanceProperty(final ResponseCallBack responseCallBack, final String propertyType) {
        String url = PROPERTY_URL;

        //switch between the different property types
        switch (propertyType) {
            case DARK_THEME:
                url += DARK_THEME;
                break;
            case SPEECH_TO_TEXT:
                url += SPEECH_TO_TEXT;
                break;
            case SYNC_ON_UPDATE:
                url += SYNC_ON_UPDATE;
                break;
            case LOGIN_CODE:
                url += LOGIN_CODE;
                break;
        }

        Call<JsonObject> callInstanceProperty = RetrofitApiCall(BASE_URL_HTTP + mInstance + BASE_URL_END_POINT).getInstanceProperty(url);
        callInstanceProperty.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                InstanceProperty ip = Preferences.getInstance().getInstanceProperty();

                switch (propertyType) {
                    case DARK_THEME:
                        ip.setDarkTheme(response.body().get("result").getAsBoolean());
                        responseCallBack.onCallSuccessObjectResult(ip, responseCallBack.RESPONSE_TYPE_INSTANCE_PROPERTY_DARK_THEME);
                        break;
                    case SPEECH_TO_TEXT:
                        ip.setSpeechToText(response.body().get("result").getAsBoolean());
                        responseCallBack.onCallSuccessObjectResult(ip, responseCallBack.RESPONSE_TYPE_INSTANCE_PROPERTY_SPEECH_TO_TEXT);
                        break;
                    case SYNC_ON_UPDATE:
                        ip.setSyncOnUpdate(response.body().get("result").getAsBoolean());
                        responseCallBack.onCallSuccessObjectResult(ip, responseCallBack.RESPONSE_TYPE_INSTANCE_PROPERTY_SYNC_ON_UPDATE);
                        break;
                    case LOGIN_CODE:
                        ip.setLoginCode(response.body().get("result").getAsString());
                        responseCallBack.onCallSuccessObjectResult(ip, responseCallBack.RESPONSE_TYPE_INSTANCE_PROPERTY_LOGIN_CODE);
                        break;
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                switch (propertyType) {
                    case DARK_THEME:
                        responseCallBack.OnCallFailed(responseCallBack.RESPONSE_TYPE_INSTANCE_PROPERTY_DARK_THEME);
                        break;
                    case SPEECH_TO_TEXT:
                        responseCallBack.OnCallFailed(responseCallBack.RESPONSE_TYPE_INSTANCE_PROPERTY_SPEECH_TO_TEXT);
                        break;
                    case SYNC_ON_UPDATE:
                        responseCallBack.OnCallFailed(responseCallBack.RESPONSE_TYPE_INSTANCE_PROPERTY_SYNC_ON_UPDATE);
                        break;
                    case LOGIN_CODE:
                        responseCallBack.OnCallFailed(responseCallBack.RESPONSE_TYPE_INSTANCE_PROPERTY_LOGIN_CODE);
                        break;
                }
            }
        });
    }

    /**
     * Update pushy token in service now
     *
     * @param systemId pass system id
     * @param token    pass pushy token
     * @throws IOException
     */
    public void registerPushyToken(String systemId, String token) throws IOException {
        String url = PUSHY_TOKEN_URL + systemId;
        JsonObject body = new JsonObject();
        body.addProperty("token", token);
        final Call<JsonObject> register = RetrofitApiCall(BASE_URL_HTTP + mInstance + BASE_URL_END_POINT).registerPushyToken(url, body);

        register.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.d(TAG, "Pushy: " + response.code());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

//        register.execute();
    }

    /**
     * authentication of the api
     *
     * @param responseCallBack to notify success or failure
     */
    public void authenticate(final ResponseCallBack responseCallBack) {
        Call<JsonObject> callAuthenticate = RetrofitApiCall(BASE_URL_HTTP + mInstance + BASE_URL_END_POINT).authenticate(AUTH_URL);
        callAuthenticate.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    responseCallBack.onCallSuccessObjectResult(null, responseCallBack.RESPONSE_TYPE_AUTHENTICATE);
                } else {
                    responseCallBack.OnCallFailed(response.code());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                responseCallBack.OnCallFailed(0);
            }
        });
    }

    /**
     * set the mInstance for service now
     *
     * @param mInstance the mInstance example: ven02380
     */
    public void setInstance(String mInstance) {
        this.mInstance = mInstance;
    }

    /**
     * set up the basic auth
     *
     * @param username set username
     * @param password set password
     */
    public void setBasicAuthCredentials(String username, String password) {
        mBasicAuthInterceptor = new BasicAuthInterceptor(username, password);
    }

    /**
     * Send translation to service now
     *
     * @param systemId
     * @param voiceTranlation
     * @throws JSONException
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void postVoiceTranslation(String systemId, String voiceTranlation) throws JSONException {
        //Setup URL
        String url = TEXT_TO_SPEECH_SAVE_URL + systemId;

        //Setup body of the JSON-Request
        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("text", voiceTranlation);
        RequestBody jsonBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());

        Call<ResponseBody> response = RetrofitApiCall(BASE_URL_HTTP + mInstance + BASE_URL_END_POINT).postVoiceTranslation(url, jsonBody);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("voice", "succes - " + response.code() + response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("voice", "failed - " + t.getMessage());
            }
        });
    }
}
