package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReservableUnit {
    @SerializedName("sys_id")
    @Expose
    private String systemId;

    @SerializedName("name")
    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }
}
