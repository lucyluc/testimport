
package models;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reservation implements Comparable {

    @SerializedName("end_at")
    @Expose
    private ServiceNowData endAt;

    @SerializedName("reservee_internal")
    @Expose
    private ServiceNowData reserveeInternal;

    @SerializedName("reservable")
    @Expose
    private ServiceNowData reservable;

    @SerializedName("start_at")
    @Expose
    private ServiceNowData startAt;

    @SerializedName("sys_id")
    @Expose
    private ServiceNowData sysId;

    public ServiceNowData getEndAt() {
        return endAt;
    }

    public ServiceNowData getReserveeInternal() {
        return reserveeInternal;
    }

    public ServiceNowData getStartAt() {
        return startAt;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return startAt.getDisplayValue().compareTo(((Reservation) o).startAt.getDisplayValue());
    }
}
