package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckInStatus {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("duration")
    @Expose
    private String duration;

    @SerializedName("reservee_internal")
    @Expose
    private String reserveeInternal;

    public static final String MEETING_SET_TO_IN_PROGRESS = "meeting_set_to_in_progress";
    public static final String MEETING_SET_TO_IN_COMPLETED = "meeting_set_to_in_completed";
    public static final String MEETING_SET_TO_IN_COMPLETED_AND_ENDED_THE_MEETING = "meeting_set_to_in_completed_and_ended_the_meeting";
    public static final String MEETING_WAS_FINISHED_BUT_NOW_SET_TO_IN_PROGRESS = "meeting_was_finished_but_now_set_to_in_progress";
    public static final String COULD_NOT_SET_MEETING_STATUS = "could_not_set_meeting_status";
    public static final String RESERVATION_ACTIVE_AFTER_SECONDS = "reservation_active_after_seconds";
    public static final String NO_ACTIVE_RESERVATION_FOUND = "no_active_reservation_found";
    public static final String USER_NOT_FOUND = "user_not_found";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        switch (status) {
            case MEETING_SET_TO_IN_PROGRESS:
                this.status = MEETING_SET_TO_IN_PROGRESS;
                break;
            case MEETING_SET_TO_IN_COMPLETED:
                this.status = MEETING_SET_TO_IN_COMPLETED;
                break;
            case MEETING_SET_TO_IN_COMPLETED_AND_ENDED_THE_MEETING:
                this.status = MEETING_WAS_FINISHED_BUT_NOW_SET_TO_IN_PROGRESS;
                break;
            case MEETING_WAS_FINISHED_BUT_NOW_SET_TO_IN_PROGRESS:
                this.status = MEETING_WAS_FINISHED_BUT_NOW_SET_TO_IN_PROGRESS;
                break;
            case COULD_NOT_SET_MEETING_STATUS:
                this.status = COULD_NOT_SET_MEETING_STATUS;
                break;
            case RESERVATION_ACTIVE_AFTER_SECONDS:
                this.status = RESERVATION_ACTIVE_AFTER_SECONDS;
                break;
            case USER_NOT_FOUND:
                this.status = USER_NOT_FOUND;
                break;
            default:
                this.status = NO_ACTIVE_RESERVATION_FOUND;
                break;
        }
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getReserveeInternal() {
        return reserveeInternal;
    }

    public void setReserveeInternal(String reserveeInternal) {
        this.reserveeInternal = reserveeInternal;
    }

    public boolean isUnitOccupied() {
        return status.equals(CheckInStatus.MEETING_SET_TO_IN_PROGRESS)
                || status.equals(CheckInStatus.MEETING_WAS_FINISHED_BUT_NOW_SET_TO_IN_PROGRESS);
    }

    public boolean isReservationAvailable() {
        return status.equals(CheckInStatus.COULD_NOT_SET_MEETING_STATUS)
                || status.equals(CheckInStatus.RESERVATION_ACTIVE_AFTER_SECONDS)
                || status.equals(CheckInStatus.USER_NOT_FOUND)
                || status.equals(CheckInStatus.NO_ACTIVE_RESERVATION_FOUND);
    }
}
