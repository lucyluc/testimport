package models;

/**
 * Created by Aschwin Bruyning on 11-6-2018.
 */

public class VoiceTranslation {
    final String text;

    public VoiceTranslation(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
