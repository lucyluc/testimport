package models;

public class InstanceProperty {

    private boolean darkTheme;
    private boolean syncOnUpdate;
    private boolean speechToText;
    private String loginCode;

    public InstanceProperty() {}

    public boolean isDarkTheme() {
        return darkTheme;
    }

    public void setDarkTheme(boolean darkTheme) {
        this.darkTheme = darkTheme;
    }

    public boolean isSyncOnUpdate() {
        return syncOnUpdate;
    }

    public void setSyncOnUpdate(boolean syncOnUpdate) {
        this.syncOnUpdate = syncOnUpdate;
    }

    public boolean isSpeechToText() {
        return speechToText;
    }

    public void setSpeechToText(boolean speechToText) {
        this.speechToText = speechToText;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }
}
