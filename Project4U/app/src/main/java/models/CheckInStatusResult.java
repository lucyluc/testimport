package models;

import com.google.gson.annotations.SerializedName;

public class CheckInStatusResult {

    @SerializedName("result")
    private CheckInStatus responseResult;

    public CheckInStatus getResponseResult() {
        return responseResult;
    }
}
