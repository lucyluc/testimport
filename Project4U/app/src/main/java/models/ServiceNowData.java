package models;

import com.google.gson.annotations.SerializedName;

public class ServiceNowData {

    @SerializedName("value")
    private String value;

    @SerializedName("display_value")
    private String displayValue;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }
}
