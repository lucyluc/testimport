package models;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class ReservationResult {

    @SerializedName("result")
    private ArrayList<Reservation> reservations;

    public ArrayList<Reservation> getReservations() {
        return reservations;
    }


}
