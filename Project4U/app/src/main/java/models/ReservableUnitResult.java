package models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReservableUnitResult {

    @SerializedName("result")
    private ArrayList<ReservableUnit> results;

    public ArrayList<ReservableUnit> getResults() {
        return results;
    }
}
