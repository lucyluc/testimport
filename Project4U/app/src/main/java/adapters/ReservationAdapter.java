package adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plat4mation.project4u.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import models.Reservation;

import static util.Utils.parseDate;

public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.ViewHolder> {

    public List<Reservation> mListReservation;

    public ReservationAdapter(List reservations) {
        this.mListReservation = reservations;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_main_schedule_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Reservation reservation = mListReservation.get(position);
        holder.mTextViewName.setText(reservation.getReserveeInternal().getDisplayValue());
        holder.mTextViewEndTime.setText(parseDate(reservation.getEndAt().getDisplayValue()));
        holder.mTextViewStartTime.setText(parseDate(reservation.getStartAt().getDisplayValue()));
    }

    @Override
    public int getItemCount() {
        return mListReservation.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_main_text_view_name) TextView mTextViewName;
        @BindView(R.id.row_main_text_view_end_time) TextView mTextViewEndTime;
        @BindView(R.id.row_main_text_view_start_time) TextView mTextViewStartTime;
        
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}
