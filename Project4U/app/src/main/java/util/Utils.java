package util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.DateFormat;
import android.util.Base64;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextClock;
import android.widget.Toast;

import com.plat4mation.project4u.R;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static final int TYPE_TIME_HOUR = 0;
    public static final int TYPE_TIME_MINUTES = 1;
    public static final int TYPE_REST_MINUTES = 2;
    public static final int TYPE_TIME_SECONDS = 3;


    /**
     * get Mac address
     * @param context to get system service
     * @return string of mac address
     */
    public static String getMacAddress(Context context){
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiManager != null) {
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            return wInfo.getMacAddress();
        }

        return "";
    }

    /**
     * get Relative greeting
     * @param resources to get text
     * @return relative greeting
     */
    public static String getRelativeGreeting(Resources resources) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int index = 0;

        if(hour < 12) {
            index = 0;
        } else if(hour < 18) {
            index = 1;
        } else if(hour > 18) {
            index = 2;
        }

        return resources.getStringArray(R.array.greetings)[index];
    }

    /**
     * parse date
     * @param dateToParse to parse
     * @return string of parsed date
     */
    public static String parseDate(String dateToParse){

        Date date = null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(dateToParse);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(date);// 9:00
    }

    /**
     * check if internet is connect
     * @param context to get system service
     * @return boolean with result
     */
    public static boolean hasInternetConnection(Context context){
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = null;

        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * format date
     * @param context to get system format
     * @param textClockTime to set time
     * @param textClockDate to set date
     */
    public static void dateFormatConfiguration(Context context, TextClock textClockTime, TextClock textClockDate){
        if (DateFormat.is24HourFormat(context)) {
            textClockTime.setFormat24Hour("HH:mm");
            textClockDate.setFormat24Hour("dd MMM yyyy");
        } else {
            textClockTime.setFormat12Hour("hh:mm");
            textClockDate.setFormat12Hour("dd MMM yyyy");
        }
    }

    /**
     * This code hides the system bars.
     * @param activity to get the window
     */
    public static void hideSystemUI(Activity activity) {
        Window w = activity.getWindow();
        Method addPrivateFlags;
        int private_flag_force_hide_nav_bar;
        try {
            addPrivateFlags = Window.class.getMethod("addPrivateFlags", int.class);
            private_flag_force_hide_nav_bar = WindowManager.LayoutParams.class
                    .getField("PRIVATE_FLAG_FORCE_HIDE_NAV_BAR").getInt(null);
        } catch (Exception ex) {
            addPrivateFlags = null;
            private_flag_force_hide_nav_bar = 0;
        }
        if (addPrivateFlags != null) {
            try {
                addPrivateFlags.invoke(w, private_flag_force_hide_nav_bar);
            } catch (Exception ex) {
                Toast.makeText(activity, Arrays.toString(ex.getStackTrace()), Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * get Relative time from seconds
     * @param timeType type of time you want to retrieve
     * @param seconds the seconds
     * @return the time according to the timeType
     */
    public static float getRelativeTimeFromSeconds(int timeType, float seconds){
        switch (timeType) {
            case TYPE_TIME_HOUR:
                return (seconds / 60) / 60;
            case TYPE_TIME_MINUTES:
                return (seconds / 60);
            case TYPE_REST_MINUTES:
                return (seconds / 60) % 60;
            case TYPE_TIME_SECONDS:
                return seconds;
        }

        return 0;
    }

    /**
     * get Ip address
     * @param context to get system service
     * @return string of ip address
     */
    public static String getIp(Context context){
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        int ipAddress = wInfo.getIpAddress();

        return String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
    }

    /**
     * encrypt a string
     * @param input to encrypt
     * @return encrypted string
     */
    public static String encrypt(String input) {
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    /**
     * decrypt a string
     * @param input to decrypt
     * @return decrypted string
     */
    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

    /**
     * @return current day time
     */
    public static String currentDayTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                .format(Calendar.getInstance().getTime());
    }

    /**
     * compare date
     * @param date to compare
     * @return int different of compare
     * @throws ParseException
     */
    public static int compareDate(String date) throws ParseException {
        int result = 0;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Date endTime = simpleDateFormat.parse(date);
        Date currentDayTime = simpleDateFormat.parse(currentDayTime());

        if (currentDayTime.after(endTime)) {
            result = 1;
        }

        if (currentDayTime.before(endTime)) {
            result = -1;
        }

        if (currentDayTime.equals(endTime)) {
            result = 0;
        }

        return result;
    }

    /**
     * set chosen theme
     * @param context to set theme
     * @param theme to change
     */
    public static void setThemePreferences(Context context, boolean theme){
        if (theme) {
            context.setTheme(R.style.plat4mationDark);
        } else {
            context.setTheme(R.style.plat4mationLight);
        }
    }
}
