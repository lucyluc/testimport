package util;

import models.InstanceProperty;

public class Preferences {

    private static InstanceProperty mInstanceProperty;
    private static Preferences sInstance;


    public static Preferences getInstance() {
        if (sInstance == null) {
            sInstance = new Preferences();
        }

        return sInstance;
    }

    public InstanceProperty getInstanceProperty() {
        if (mInstanceProperty == null)
            return mInstanceProperty = new InstanceProperty();

        return mInstanceProperty;
    }

    public void setInstanceProperty(InstanceProperty property) {
        mInstanceProperty = property;
    }
}
