package util;

import models.ReservableUnit;

public class UnitManager {

    private static UnitManager sInstance;
    private ReservableUnit mReservableUnit;


    public static UnitManager getInstance() {
        if (sInstance == null) {
            sInstance = new UnitManager();
        }

        return sInstance;
    }

    public ReservableUnit getReservableUnit() {
        return mReservableUnit;
    }

    public void setReservableUnit(ReservableUnit reservableUnit) {
        this.mReservableUnit = reservableUnit;
    }
}
