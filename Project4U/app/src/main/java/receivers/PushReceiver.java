package receivers;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.util.Log;

import java.lang.ref.WeakReference;

import activities.MainActivity;

import static activities.MainActivity.NAME_SHARED_PREFERENCES;

public class PushReceiver extends BroadcastReceiver {
    private static final String TAG = "PushReceiver";
    public static final String MESSAGE_UPDATED = "updated";

    private static WeakReference<MainActivity> mMainActivityWeakReference;

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sharedPreferences = mMainActivityWeakReference.get()
                .getSharedPreferences(NAME_SHARED_PREFERENCES, Context.MODE_PRIVATE);

        if (sharedPreferences.getBoolean("enable_sync_on_update", false)) {
            String receivedMessage;

            if (intent.getStringExtra("message") != null) {
                receivedMessage = intent.getStringExtra("message");
                if (receivedMessage.equals(MESSAGE_UPDATED)) {
                    Log.d(TAG, "onReceive: " + receivedMessage);
                    mMainActivityWeakReference.get().getReservations();
                }
            }
        }
    }

    public void setMainActivity(MainActivity activity) {
        mMainActivityWeakReference = new WeakReference<>(activity);
    }
}
