This is the repository for the Plat4mation team. 
In this repository we'll upload every code we write. 
Our documents can be found on our teamsite: https://plat4mationts.wordpress.com

Our team consists of: 
Youssra Outelli
Mariam el Mojahid
Jesper Vink - Team leader
Justine van Mil - Co-leader
Sam Janga
Aschwin Bruyning
Ismail Sarican
Dennis Eendragt